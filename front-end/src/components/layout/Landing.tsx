import React from 'react'

const Landing = () => {
    return (
        <section className="landing">
            <div className="overlay">
                <div className="landing-inner">
                    <h1>App</h1>
                    <p>Description of the app</p>
                    <div className="buttons">
                        <a href="register.html">Sign Up</a>
                        <a href="login.html">Login</a>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Landing
