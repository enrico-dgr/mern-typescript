import { Express, Request, Response } from "express";
import express = require("express");
const connectDB = require("./database/db");

export class Server {
  private app: Express;

  constructor(app: Express) {
    this.app = app;

    connectDB();

    // Init Middleware
    app.use(express.json({ limit: "200kb" }));

    this.app.get("/", (req: Request, res: Response): void => {
      res.send("API running");
    });

    // Define Routes
    this.app.use("/api/users", require("./routes/api/users"));
    this.app.use("/api/auth", require("./routes/api/auth"));
  }

  public listen(PORT: number | string): void {
    this.app.listen(PORT, () =>
      console.log(`Server listening on port ${PORT}!`)
    );
  }
}
// const app = express();

// // Connect Database
// connectDB();

// // Init Middleware
// app.use(express.json({ limit: "200kb" }));

// app.get("/", (req, res) => res.send("API Running"));

// // Define Routes
// app.use("/api/users", require("./routes/api/users"));
// app.use("/api/auth", require("./routes/api/auth"));

// const PORT = process.env.PORT || 5000;

// app.listen(PORT, () => console.log(`Server started on port ${PORT}`));
