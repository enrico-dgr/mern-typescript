import {Server} from "./src/server";
import express = require('express');
const app = express();

const PORT = process.env.PORT || 5000;

const server = new Server(app);
server.listen(PORT);